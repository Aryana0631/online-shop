#!/bin/bash
docker rm --force onlineshop
mvn clean package -DskipTests
docker image build -t onlineshop .
docker container run --name onlineshop -p 8080:8080 -d onlineshop