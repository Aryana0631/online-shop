FROM openjdk:17-jdk-alpine
MAINTAINER aryana0631
COPY target/onlineshop-0.0.1-SNAPSHOT.jar onlineshop-server-1.0.0.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/onlineshop-server-1.0.0.jar"]