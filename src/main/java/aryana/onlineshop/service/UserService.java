package aryana.onlineshop.service;

import aryana.onlineshop.entity.User;
import aryana.onlineshop.model.UserValidateModel;
import aryana.onlineshop.rabbitmq.Producer;
import aryana.onlineshop.repository.interfaces.UserRepository;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private PasswordValidator passwordValidator;

    @Autowired
    private EmailValidator emailValidator;

    @Autowired
    private Producer rabbitMQService;

    @Autowired
    private Gson gson;

    @Autowired
    private JwtService jwtService;


    @Autowired
    private PhoneNumberValidator phoneNumberValidator;

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public User findByUserName(String userName) {
        return userRepository.findByUserName(userName);
    }

    public User findUserByToken(String token) {
        return findByUserName(jwtService.extractUsername(token));
    }

    public User findById(String id) {
        Optional<User> userOptional = userRepository.findById(id);
        return userOptional.orElse(null);
    }

    private UserValidateModel validateUser(User user) {
        if (user.getPassword() == null || (user.getId() == null && !passwordValidator.validate(user.getPassword()))) {
            return new UserValidateModel(null, false, "Password Is Not Valid");
        }
        if (user.getPhoneNumber() == null || !phoneNumberValidator.validate(user.getPhoneNumber())) {
            return new UserValidateModel(null, false, "PhoneNumber Is Not Valid");
        }
        if (user.getEmail() == null || !emailValidator.validate(user.getEmail())) {
            return new UserValidateModel(null, false, "Email Is Not Valid");
        }
        if (user.getUserName() == null || (user.getId() == null && userRepository.findByUserName(user.getUserName()) != null)) {
            return new UserValidateModel(null, false, "Username Already Exists in the System or Is Not Valid.");
        }
        return new UserValidateModel(user, true, "Validated");
    }


    public UserValidateModel saveUser(User user) {
        UserValidateModel userValidateModel = validateUser(user);
        if (!userValidateModel.isValidated()) {
            return userValidateModel;
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return new UserValidateModel(userRepository.save(user), true, "Saved");
    }

    public User login(String userName, String password) {
        User user = findByUserName(userName);
        if (user == null) {
            return null;
        }
        boolean matchPassword = passwordEncoder.matches(password, user.getPassword());
        if (matchPassword) {
            return user;
        } else {
            return null;
        }
    }

    public UserValidateModel updateUserInfo(User updatedUser) {
        User userToChange = findByUserName(updatedUser.getUserName());
        if (userToChange == null) {
            return new UserValidateModel(null, false, "User Not Found");
        }
        userToChange.setFirstName(updatedUser.getFirstName());
        userToChange.setLastName(updatedUser.getLastName());
        userToChange.setPhoneNumber(updatedUser.getPhoneNumber());
        userToChange.setEmail(updatedUser.getEmail());
        UserValidateModel userValidateModel = validateUser(userToChange);
        if (!userValidateModel.isValidated()) {
            return userValidateModel;
        }
        return new UserValidateModel(userRepository.save(userToChange), true, "User Updated");
    }

    public void updatePaymentUser(UserValidateModel userValidateModel){
        if(userValidateModel.isValidated()){
            rabbitMQService.sendMessage(gson.toJson(userValidateModel.getUser()));
        }
    }
}
