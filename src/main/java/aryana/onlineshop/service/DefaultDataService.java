package aryana.onlineshop.service;

import aryana.onlineshop.entity.Order;
import aryana.onlineshop.entity.Product;
import aryana.onlineshop.model.ProductCategory;
import aryana.onlineshop.model.Role;
import aryana.onlineshop.entity.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DefaultDataService {

    public User getDefaultUser() {
        return new User("Aryana", "Azadi", "Aryana0631", "Password", "AryanaAzadiLive@gmail.com", "09301661152", Role.USER);
    }

    public User getDefaultValidUser() {
        return new User("Aryana", "Azadi", "Aryana0631", "!@Password1234", "AryanaAzadiLive@gmail.com", "+449301661152", Role.USER);
    }

    public User getDefaultValidAdminUser() {
        return new User("Aryana", "Azadi", "Aryana0631", "!@Password1234", "AryanaAzadiLive@gmail.com", "+449301661152", Role.ADMIN);
    }

    public Product getDefaultProduct() {
        return new Product("PlayStation 5", "PlayStation 5 Standard Edition", ProductCategory.DIGITALDEVICE, 500);
    }

    public Product getDefaultProduct1() {
        return new Product("Xbox Series X", "Xbox Series X", ProductCategory.DIGITALDEVICE, 500);
    }

    public Order getDefaultOrder(){
        List<Product> productList = new ArrayList<>();
        productList.add(getDefaultProduct());
        productList.add(getDefaultProduct1());
        return new Order(getDefaultValidUser(), productList);
    }
}
