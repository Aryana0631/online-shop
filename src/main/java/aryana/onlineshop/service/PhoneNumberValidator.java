package aryana.onlineshop.service;

import org.springframework.stereotype.Service;

@Service
public class PhoneNumberValidator {


    public boolean validate(String phoneNumber) {
        if (!phoneNumber.startsWith("+1") && !phoneNumber.startsWith("+44") && !phoneNumber.startsWith("+49")) {
            return false;
        }
        if (phoneNumber.length() != 13 && !phoneNumber.startsWith("+1")) {
            return false;
        }
        if(phoneNumber.startsWith("+1") && phoneNumber.length() != 12){
            return false;
        }
        return true;
    }
}
