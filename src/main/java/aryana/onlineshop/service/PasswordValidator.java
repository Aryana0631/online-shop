package aryana.onlineshop.service;

import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

@Service
public class PasswordValidator {


    public boolean validate(String password) {
        String regexPattern = "^(?=.*[!@#$%^&-+=.()]{2}).{10,100}$";

        return Pattern.compile(regexPattern)
                .matcher(password)
                .matches();
    }
}
