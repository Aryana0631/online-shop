package aryana.onlineshop.service;

import aryana.onlineshop.entity.Product;
import aryana.onlineshop.repository.interfaces.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public Product createNewProduct(Product product) {
        if (product == null) {
            return null;
        }
        return productRepository.save(product);
    }

    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    public ArrayList<Product> idsToProducts(List<Product> productIds){
        ArrayList<Product> products = new ArrayList<>();
        for (Product product: productIds
             ) {
            Product newProduct = productRepository.findById(product.getId()).orElse(null);
            if(newProduct == null){
                return null;
            }
            products.add(newProduct);
        }
        return products;
    }


}
