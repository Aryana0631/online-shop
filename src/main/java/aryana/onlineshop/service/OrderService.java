package aryana.onlineshop.service;

import aryana.onlineshop.entity.Order;
import aryana.onlineshop.entity.Product;
import aryana.onlineshop.entity.User;
import aryana.onlineshop.repository.interfaces.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    public double totalValueOfOrder(List<Product> products) {
        int totalValue = 0;
        for (Product product : products
        ) {
            totalValue += product.getPrice();
        }
        return totalValue;
    }

    public void createOrder(User user, ArrayList<Product> products, boolean payResult) {
        Order order = new Order();
        order.setUser(user);
        order.setProducts(products);
        order.setPayed(payResult);
        if(payResult){
            order.setPayedAt(new Date());
        }
        orderRepository.save(order);
    }
}
