package aryana.onlineshop.repository.interfaces;

import aryana.onlineshop.entity.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface UserRepository extends MongoRepository<User, String> {

    public Optional<User> findById(String id);
    public User findByUserName(String userName);

}
