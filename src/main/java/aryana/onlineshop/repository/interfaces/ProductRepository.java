package aryana.onlineshop.repository.interfaces;

import aryana.onlineshop.entity.Product;
import aryana.onlineshop.entity.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface ProductRepository extends MongoRepository<Product, String>{

    public Optional<Product> findById(String id);

    public Product findByName(String name);

}
