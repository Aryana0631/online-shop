package aryana.onlineshop.repository.interfaces;

import aryana.onlineshop.entity.Order;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface OrderRepository extends MongoRepository<Order, String> {
}
