package aryana.onlineshop.entity;

import aryana.onlineshop.model.ProductCategory;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "product")
@Getter
@Setter
public class Product {

    @Id
    private String id;
    private String name;
    private String description;
    private ProductCategory category;
    private double price;
    private Date createdAt = new Date();

    public Product(String name) {
        this.name = name;
    }

    public Product(String name, String description, ProductCategory category, double price) {
        this.name = name;
        this.description = description;
        this.category = category;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", category=" + category +
                ", price=" + price +
                ", createdAt=" + createdAt +
                '}';
    }
}
