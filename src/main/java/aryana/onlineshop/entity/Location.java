package aryana.onlineshop.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "location")
@Getter
@Setter
public class Location {

    @Id
    private int id;
    private String country;
    private String city;
    private int x;
    private int y;

}
