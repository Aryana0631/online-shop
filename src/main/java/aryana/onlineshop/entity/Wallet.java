package aryana.onlineshop.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "wallet")
@Getter
@Setter
public class Wallet {

    private int id;
    @DBRef
    private User user;
    private double balance;


}
