package aryana.onlineshop.rabbitmq;

import aryana.onlineshop.OnlineShopApplication;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
@Service
public class Producer implements CommandLineRunner {

    private final RabbitTemplate rabbitTemplate;

    public Producer(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Sending message...");
    }

    public String sendMessage(String message) {
        rabbitTemplate.convertAndSend(OnlineShopApplication.topicExchangeName, "online-shop", message);
        return "Message sent: " + message;
    }

}
