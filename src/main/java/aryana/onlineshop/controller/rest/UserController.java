package aryana.onlineshop.controller.rest;


import aryana.onlineshop.entity.User;
import aryana.onlineshop.model.ResponseMessage;
import aryana.onlineshop.model.UserValidateModel;
import aryana.onlineshop.service.JwtService;
import aryana.onlineshop.rabbitmq.Producer;
import aryana.onlineshop.service.UserService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
@RestController
public class UserController {


    @Autowired
    private JwtService jwtService;

    @Autowired
    private UserService userService;


    @Autowired
    private Producer rabbitMQService;

    @Autowired
    private Gson gson;

    @GetMapping("/user")
    ResponseEntity<List<User>> all(@RequestHeader Map<String, String> headers) {
        if(jwtService.isTokenExpired(headers.get("authorization"))){
            return new ResponseEntity<>(new ArrayList<>(), HttpStatus.OK);
        }
        return new ResponseEntity<>(userService.getAllUsers(), HttpStatus.OK);
    }

    @GetMapping("/current_user")
    ResponseEntity<User> currentUser(@RequestHeader Map<String, String> headers) {
        return new ResponseEntity<>(userService.findUserByToken(headers.get("authorization")), HttpStatus.OK);
    }

    @PostMapping("/user/create")
    ResponseEntity<?> createUser(@RequestBody User newUser) {
        UserValidateModel userValidateModel = userService.saveUser(newUser);
        userService.updatePaymentUser(userValidateModel);
        return new ResponseEntity<>(new ResponseMessage(userValidateModel.isValidated(), userValidateModel.getMoreInfo()), HttpStatus.OK);
    }

    @GetMapping("/user/login")
    ResponseEntity<?> login(@RequestBody User user) {
        User foundUser = userService.login(user.getUserName(), user.getPassword());
        if (foundUser != null) {
            return new ResponseEntity<>(new ResponseMessage(true, jwtService.generateToken(foundUser)), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new ResponseMessage(false, "Username or Password is Wrong"), HttpStatus.OK);
        }
    }

    @PostMapping("/user/update")
    ResponseEntity<?> updateUser(@RequestHeader Map<String, String> headers, @RequestBody User user) {
        boolean isTokenValid = jwtService.validateToken(headers.get("authorization"), user);
        if(!isTokenValid){
            return new ResponseEntity<>(new ResponseMessage(false, "Token Is Not Valid"), HttpStatus.OK);
        }
        UserValidateModel changedUser = userService.updateUserInfo(user);
        userService.updatePaymentUser(changedUser);
        return new ResponseEntity<>(new ResponseMessage(changedUser.isValidated(), changedUser.getMoreInfo()), HttpStatus.OK);
    }

}
