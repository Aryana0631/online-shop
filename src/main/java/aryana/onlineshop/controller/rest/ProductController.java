package aryana.onlineshop.controller.rest;

import aryana.onlineshop.entity.Product;
import aryana.onlineshop.model.Role;
import aryana.onlineshop.entity.User;
import aryana.onlineshop.model.ResponseMessage;
import aryana.onlineshop.service.JwtService;
import aryana.onlineshop.service.ProductService;
import aryana.onlineshop.service.UserService;
import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
public class ProductController {

    @Autowired
    private ProductService productService;

    @Autowired
    private JwtService jwtService;

    @Autowired
    private UserService userService;


    @GetMapping("/product")
    ResponseEntity<List<Product>> all(@RequestHeader Map<String, String> headers) {
        if (jwtService.isTokenExpired(headers.get("authorization"))) {
            return new ResponseEntity<>(new ArrayList<>(), HttpStatus.OK);
        }
        return new ResponseEntity<>(productService.getAllProducts(), HttpStatus.OK);
    }


    @PostMapping("/product/create")
    ResponseEntity<?> createProduct(@RequestHeader Map<String, String> headers, @RequestBody Product newProduct) {
        try {
            User user = userService.findUserByToken(headers.get("authorization"));
            if (user.getRole() == Role.ADMIN) {
                productService.createNewProduct(newProduct);
                return new ResponseEntity<>(new ResponseMessage(true, newProduct.toString()), HttpStatus.OK);
            }
            return new ResponseEntity<>(new ResponseMessage(false, "Only Admins Can Create Product."), HttpStatus.OK);
        } catch (ExpiredJwtException e) {
            return new ResponseEntity<>(new ResponseMessage(false, "Please Login"), HttpStatus.OK);
        }
    }

}
