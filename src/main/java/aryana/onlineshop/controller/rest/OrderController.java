package aryana.onlineshop.controller.rest;

import aryana.onlineshop.entity.Order;
import aryana.onlineshop.entity.Product;
import aryana.onlineshop.entity.User;
import aryana.onlineshop.model.AddMoney;
import aryana.onlineshop.model.ResponseMessage;
import aryana.onlineshop.model.Transaction;
import aryana.onlineshop.repository.interfaces.OrderRepository;
import aryana.onlineshop.repository.interfaces.UserRepository;
import aryana.onlineshop.service.*;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.*;

import static aryana.onlineshop.model.PaymentServiceInfo.url;

@RestController
public class OrderController {

    @Autowired
    private RestTemplate restTemplate;


    @Autowired
    private ProductService productService;

    @Autowired
    private UserService userService;


    @Autowired
    private OrderService orderService;

    @Autowired
    private Gson gson;


    @PostMapping("/order/create")
    ResponseEntity<?> createOrder(@RequestHeader Map<String, String> headers, @RequestBody ArrayList<Product> productIds) {
        User user = userService.findUserByToken(headers.get("authorization"));
        ArrayList<Product> products = productService.idsToProducts(productIds);

        if (products == null) {
            return new ResponseEntity<>(new ResponseMessage(false, "One of the Product IDs Are Wrong"), HttpStatus.OK);
        }
        ResponseEntity<ResponseMessage> response = restTemplate.postForEntity(
                url + "/transaction/pay",
                new Transaction(user, orderService.totalValueOfOrder(products)),
                ResponseMessage.class
        );

        orderService.createOrder(user, products, Objects.requireNonNull(response.getBody()).result());

        return response;
    }

    @PostMapping("/order/add_money")
    ResponseEntity<?> addMoney(@RequestHeader Map<String, String> headers, @RequestBody AddMoney value) {
        User user = userService.findUserByToken(headers.get("authorization"));
        value.setUser(user);

        return restTemplate.postForEntity(
                url + "/transaction/add_money",
                value,
                ResponseMessage.class
        );
    }

}
