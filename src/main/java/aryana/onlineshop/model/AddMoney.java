package aryana.onlineshop.model;


import aryana.onlineshop.entity.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AddMoney {

    private User user;
    private double value;



}
