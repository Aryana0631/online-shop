package aryana.onlineshop.model;

public record ResponseMessage(boolean result, String message) {

}
