package aryana.onlineshop.model;

public enum ProductCategory {
    DIGITALDEVICE, CLOTHE, VIDEOGAME
}
