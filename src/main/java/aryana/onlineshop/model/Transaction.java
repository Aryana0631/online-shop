package aryana.onlineshop.model;

import aryana.onlineshop.entity.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Transaction {

    private User user;
    private double valueToBePayed;



}
