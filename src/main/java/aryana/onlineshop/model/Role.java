package aryana.onlineshop.model;

public enum Role {
    ADMIN, USER
}
