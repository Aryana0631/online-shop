package aryana.onlineshop.order;

import aryana.onlineshop.Config;
import aryana.onlineshop.entity.Order;
import aryana.onlineshop.entity.Product;
import org.junit.jupiter.api.Test;


import static org.assertj.core.api.Assertions.assertThat;

public class OrderServiceTest extends Config {

    @Test
    public void totalValueOfOrder() {
        Order order = defaultDataService.getDefaultOrder();
        double totalValue = orderService.totalValueOfOrder(order.getProducts());
        double calculatedTotalValue = 0;
        for (Product product : order.getProducts()
        ) {
            calculatedTotalValue += product.getPrice();
        }
        assertThat(calculatedTotalValue).isEqualTo(totalValue);
    }

}
