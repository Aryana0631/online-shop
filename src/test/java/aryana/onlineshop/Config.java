package aryana.onlineshop;

import aryana.onlineshop.repository.interfaces.ProductRepository;
import aryana.onlineshop.repository.interfaces.UserRepository;
import aryana.onlineshop.service.DefaultDataService;
import aryana.onlineshop.service.OrderService;
import aryana.onlineshop.service.ProductService;
import aryana.onlineshop.service.UserService;
import org.junit.jupiter.api.BeforeAll;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

@SpringBootTest
@Testcontainers
public class Config {


    @Container
    public static MongoDBContainer container = new MongoDBContainer(DockerImageName.parse("mongo:latest"));

    @BeforeAll
    static void initAll() {
        container.start();
    }

    @SpyBean
    protected UserRepository userRepository;

    @SpyBean
    protected ProductRepository productRepository;

    @SpyBean
    protected UserService userService;

    @SpyBean
    protected ProductService productService;

    @SpyBean
    protected DefaultDataService defaultDataService;

    @SpyBean
    protected OrderService orderService;


}
