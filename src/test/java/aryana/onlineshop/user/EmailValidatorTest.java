package aryana.onlineshop.user;

import aryana.onlineshop.Config;
import aryana.onlineshop.service.EmailValidator;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class EmailValidatorTest extends Config {

    @Test
    public void emailDomainIsWrong(){
        EmailValidator emailValidator = new EmailValidator();

        String email = "username@gmail.";

        boolean result = emailValidator.validate(email);

        assertThat(result).isEqualTo(false);
    }

    @Test
    public void emailLocalPartIsWrong(){
        EmailValidator emailValidator = new EmailValidator();

        String email = ".username..@gmail.com";

        boolean result = emailValidator.validate(email);

        assertThat(result).isEqualTo(false);
    }

    @Test
    public void emailIsCorrect(){
        EmailValidator emailValidator = new EmailValidator();

        String email = "AryanaAzadiLive@gmail.com";

        boolean result = emailValidator.validate(email);

        assertThat(result).isEqualTo(true);
    }
}
