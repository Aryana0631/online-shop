package aryana.onlineshop.user;


import aryana.onlineshop.Config;
import aryana.onlineshop.model.Role;
import aryana.onlineshop.entity.User;
import aryana.onlineshop.model.UserValidateModel;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


public class UserServiceTest extends Config {


    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.data.mongodb.uri", container::getReplicaSetUrl);
    }

    @BeforeEach
    public void deleteAllBeforeEach() {
        userRepository.deleteAll();
    }


    @Test
    public void saveUserWithNotValidPassword() {
        User user = new User("Aryana", "Azadi", "Aryana0631", "Password", "AryanaAzadiLive@gmail.com", "+449301661152", Role.USER);
        UserValidateModel model = userService.saveUser(user);
        assertThat(model.isValidated()).isEqualTo(false);
        assertThat(model.getMoreInfo()).isEqualToIgnoringCase("Password Is Not Valid");
    }

    @Test
    public void saveUserWithNotValidPhoneNumber() {
        User user = new User("Aryana", "Azadi", "Aryana0631", "!@#Az123456789", "AryanaAzadiLive@gmail.com", "09301661152", Role.USER);
        UserValidateModel model = userService.saveUser(user);
        assertThat(model.isValidated()).isEqualTo(false);
        assertThat(model.getMoreInfo()).isEqualToIgnoringCase("PhoneNumber Is Not Valid");
    }

    @Test
    public void saveUserWithNotValidEmail() {
        User user = new User("Aryana", "Azadi", "Aryana0631", "!@#Az123456789", "34..5767@.com", "+449301661152", Role.USER);
        UserValidateModel model = userService.saveUser(user);
        assertThat(model.isValidated()).isEqualTo(false);
        assertThat(model.getMoreInfo()).isEqualToIgnoringCase("Email Is Not Valid");
    }

    @Test
    public void duplicateUserNameSave() {
        UserValidateModel model1 = userService.saveUser(defaultDataService.getDefaultValidUser());
        UserValidateModel model2 = userService.saveUser(defaultDataService.getDefaultValidUser());
        assertThat(model1.isValidated()).isEqualTo(true);
        assertThat(model2.isValidated()).isEqualTo(false);
    }

    @Test
    public void saveValidatedUser() {
        User savedUser = userService.saveUser(defaultDataService.getDefaultValidUser()).getUser();
        assertThat(savedUser).isNotNull();
    }


    @Test
    public void findByUserName() {
        User savedUser = userRepository.save(defaultDataService.getDefaultUser());
        User foundUser = userService.findByUserName(savedUser.getUserName());
        assertThat(foundUser.getUserName()).isEqualTo(savedUser.getUserName());
    }

    @Test
    public void findByUserNameWhenDoesntExists() {
        User foundUser = userService.findByUserName("Test User Name");
        assertThat(foundUser).isEqualTo(null);
    }

    @Test
    public void findById() {
        User savedUser = userRepository.save(defaultDataService.getDefaultUser());
        User foundUser = userService.findById(savedUser.getId());
        assertThat(foundUser.getId()).isEqualTo(savedUser.getId());
    }

    @Test
    public void findByIdWhenDoesntExists() {
        User foundUser = userService.findById("346347347");
        assertThat(foundUser).isEqualTo(null);
    }

    @Test
    public void getAllUsers() {
        User savedUser1 = userRepository.save(defaultDataService.getDefaultUser());
        User savedUser2 = userRepository.save(new User("Aryana1", "Azadi1", "Aryana1", "Password", "AryanaAzadiLive1@gmail.com", "09301661152", Role.USER));
        User savedUser3 = userRepository.save(new User("Aryana2", "Azadi2", "Aryana2", "Password", "AryanaAzadiLive2@gmail.com", "09301661152", Role.USER));
        List<User> users = userService.getAllUsers();
        assertThat(users.size()).isEqualTo(3);
    }

    @Test
    public void getAllUsersWhenEmpty() {
        List<User> users = userService.getAllUsers();
        assertThat(users.size()).isEqualTo(0);
    }

    @Test
    public void loginByRightUserPassword() {
        User savedUser = userService.saveUser(defaultDataService.getDefaultValidUser()).getUser();
        User user = userService.login(defaultDataService.getDefaultValidUser().getUserName(), defaultDataService.getDefaultValidUser().getPassword());
        assertThat(user.getUserName()).isEqualTo(savedUser.getUserName());
    }

    @Test
    public void loginByWrongUserName() {
        User savedUser = userService.saveUser(defaultDataService.getDefaultValidUser()).getUser();
        User user = userService.login("Test Wrong User Name", defaultDataService.getDefaultValidUser().getPassword());
        assertThat(user).isNull();
    }


    @Test
    public void loginByWrongPassword() {
        User savedUser = userService.saveUser(defaultDataService.getDefaultValidUser()).getUser();
        User user = userService.login(defaultDataService.getDefaultValidUser().getUserName(), "Test Wrong Password");
        assertThat(user).isNull();
    }

    @Test
    public void updateUser() {
        User toBeUpdatedUser = userService.saveUser(defaultDataService.getDefaultValidUser()).getUser();
        toBeUpdatedUser.setEmail("AryanaAzadiLive@hotmail.com");
        toBeUpdatedUser.setFirstName("Aryana1");
        toBeUpdatedUser.setLastName("Azadi1");
        toBeUpdatedUser.setPhoneNumber("+19169301152");
        User changedUser = userService.updateUserInfo(toBeUpdatedUser).getUser();
        assertThat(changedUser.getId()).isEqualToIgnoringCase(toBeUpdatedUser.getId());
    }

    @Test
    public void updateUserNoUserFound() {
        User toBeUpdatedUser = defaultDataService.getDefaultValidUser();
        User changedUser = userService.updateUserInfo(toBeUpdatedUser).getUser();
        assertThat(changedUser).isNull();
    }


}
