package aryana.onlineshop.user;

import aryana.onlineshop.Config;
import aryana.onlineshop.service.PasswordValidator;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class PasswordValidatorTest extends Config {

    @Test
    public void passwordHasNoSpecialChar(){
        PasswordValidator passwordValidator = new PasswordValidator();
        String password = "5674574574";
        boolean result = passwordValidator.validate(password);
        assertThat(result).isEqualTo(false);
    }

    @Test
    public void passwordHasOneSpecialChar(){
        PasswordValidator passwordValidator = new PasswordValidator();
        String password = "@674574574";
        boolean result = passwordValidator.validate(password);
        assertThat(result).isEqualTo(false);
    }

    @Test
    public void passwordLengthIsLessThanTen(){
        PasswordValidator passwordValidator = new PasswordValidator();
        String password = "@#53236";
        boolean result = passwordValidator.validate(password);
        assertThat(result).isEqualTo(false);
    }

    @Test
    public void passwordIsCorrect(){
        PasswordValidator passwordValidator = new PasswordValidator();
        String password = "@.523fjsdhen4fe";
        boolean result = passwordValidator.validate(password);
        assertThat(result).isEqualTo(true);
    }

}
