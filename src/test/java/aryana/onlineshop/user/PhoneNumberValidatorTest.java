package aryana.onlineshop.user;

import aryana.onlineshop.Config;
import aryana.onlineshop.service.PhoneNumberValidator;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;

public class PhoneNumberValidatorTest extends Config {

    // Pre number not right
    @Test
    public void preNumberNotCorrect() {
        PhoneNumberValidator phoneNumberValidator = new PhoneNumberValidator();

        String phoneNumber = "+989169373252";

        boolean result = phoneNumberValidator.validate(phoneNumber);

        assertThat(result).isEqualTo(false);
    }

    // Number length is more than 13
    @Test
    public void lengthIsNotEqualToThirteen(){
        PhoneNumberValidator phoneNumberValidator = new PhoneNumberValidator();

        String phoneNumber = "+449169363473252";

        boolean result = phoneNumberValidator.validate(phoneNumber);

        assertThat(result).isEqualTo(false);
    }

    @Test
    public void uSANumberCorrect(){
        PhoneNumberValidator phoneNumberValidator = new PhoneNumberValidator();

        String phoneNumber = "+19169373252";

        boolean result = phoneNumberValidator.validate(phoneNumber);

        assertThat(result).isEqualTo(true);
    }

    // Number is correct
    @Test
    public void numberIsCorrect(){
        PhoneNumberValidator phoneNumberValidator = new PhoneNumberValidator();

        String phoneNumber = "+449169373252";

        boolean result = phoneNumberValidator.validate(phoneNumber);

        assertThat(result).isEqualTo(true);
    }


}
