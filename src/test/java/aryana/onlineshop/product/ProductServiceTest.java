package aryana.onlineshop.product;

import aryana.onlineshop.Config;
import aryana.onlineshop.entity.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ProductServiceTest extends Config {

    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.data.mongodb.uri", container::getReplicaSetUrl);
    }

    @BeforeEach
    public void deleteAllBeforeEach() {
        productRepository.deleteAll();
    }


    @Test
    public void addProduct() {
        Product newProduct = productService.createNewProduct(defaultDataService.getDefaultProduct());
        assertThat(newProduct).isNotNull();
    }

    @Test
    public void addEmptyProduct() {
        Product newProduct = productService.createNewProduct(null);
        assertThat(newProduct).isNull();
    }

    @Test
    public void getAllProduct(){
        Product savedProduct1 = productRepository.save(defaultDataService.getDefaultProduct());
        Product savedProduct2 = productRepository.save(defaultDataService.getDefaultProduct());
        Product savedProduct3 = productRepository.save(defaultDataService.getDefaultProduct());
        List<Product> products = productService.getAllProducts();
        assertThat(products.size()).isEqualTo(3);
    }

    @Test
    public void getAllProductWhenEmpty(){
        List<Product> products = productService.getAllProducts();
        assertThat(products.size()).isEqualTo(0);
    }


}
